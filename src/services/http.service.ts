import axios from 'axios';
import { Game } from '@/models/game';
import { Account } from '@/models/account';
import { parseTile } from '@/models/tile';

export class HttpService {
  baseUrl = '/api';
  account: Account | null = null;

  get isLoggedIn() {
    return this.account !== null;
  }

  constructor() {
    axios.defaults.baseURL = this.baseUrl;
  }

  async login(email: string, password: string): Promise<boolean> {
    try {
      const response = await axios.get('/account', {
        headers: {
          Authorization: `Basic ${btoa(`${email}:${password}`)}`
        }
      });
      axios.defaults.headers.common.Authorization = `Basic ${btoa(`${email}:${password}`)}`;
      this.account = response.data;
      return true;
    } catch (err) {
      return false;
    }
  }

  async getGames(): Promise<Game[]> {
    const response = await axios.get<any[]>('/games');
    return response.data.map((game: any) => {
      game.tile_rack = game.tile_rack.map(parseTile);
      game.board = game.board.map((set: any) => {
        set.tiles = set.tiles.map(parseTile);
        return set;
      });
      return game;
    });
  }
}
