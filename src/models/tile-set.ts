import { Tile } from './tile';

export interface TileSet {
  tiles: Tile[];
  level: number;
  x: number;
}
