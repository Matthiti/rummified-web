export enum Color {
  Black  = '#000',
  Red    = '#dc3545',
  Yellow = '#ffc107',
  Blue   = '#007bff'
}

export interface Tile {
  isJoker: boolean;
  color: Color | null;
  value: number | null;
  variant: string;
}

export function parseTile(inputTile: string): Tile {
  if (inputTile.startsWith('J')) {
    return {
      isJoker: true,
      color: null,
      value: null,
      variant: inputTile.charAt(inputTile.length - 1)
    };
  }

  let color: Color;
  switch (inputTile.charAt(0)) {
    case 'A':
      color = Color.Black;
      break;
    case 'B':
      color = Color.Red;
      break;
    case 'C':
      color = Color.Yellow;
      break;
    default:
      color = Color.Blue;
      break;
  }

  return {
    isJoker: false,
    color: color,
    value: +inputTile.substring(1, inputTile.length - 1),
    variant: inputTile.charAt(inputTile.length - 1)
  };
}
