export interface Account {
  id: number;
  username: string;
  email: string;
  has_payed: boolean;
  premium_end: string;
  new_account: boolean;
}
