export interface Participant {
  account_id: number;
  username: string;
  status: string;
  played_30: boolean,
  score: number,
  position: number
}